<?php

namespace Drupal\staged_content;

use Drupal\Component\Plugin\PluginManagerInterface;

/**
 * Detects and resolves which marker a given entity uses.
 */
interface MarkerDetectorManagerInterface extends PluginManagerInterface {

}

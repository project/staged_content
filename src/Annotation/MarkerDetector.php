<?php

namespace Drupal\staged_content\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a Icon Set annotation object.
 *
 * Plugin Namespace: Plugin\StagedContent\Marker.
 *
 * @ingroup third_party
 *
 * @Annotation
 */
class MarkerDetector extends Plugin {

}
